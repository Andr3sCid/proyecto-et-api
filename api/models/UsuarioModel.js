const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const usuarioSchema = new Schema({
  nombre_usuario: {
    type: String,
    required: [true, "Nombre Obligatorio"],
    unique: true,
  },
  contrasena: { type: String, required: [true, "Contraseña Obligatorio"] },
});

//conversión a modelo
const Usuario = mongoose.model("Usuario", usuarioSchema);
module.exports = Usuario;
