const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const sesionSchema = new Schema({
  usuario: { type: Schema.Types.ObjectId, ref:'Usuario', required: [true,"id usuario requerido"] },
  fecha: { type: String, required: [true,"fecha requerida"] },
  score: { type: Number, required: [true,"puntaje requerido"] },
  duration: { type: Number, required: [true,"duracion requerida"] }, 
});

//conversión a modelo
const Sesiones = mongoose.model("Sesiones", sesionSchema);
module.exports = Sesiones;
