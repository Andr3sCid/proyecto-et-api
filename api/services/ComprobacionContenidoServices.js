const express = require("express");
const router = express.Router();
//Importar el modelo de Sesion
const SesionModel = require("../models/SesionModels");
//Importar el modelo de Usuario
const UsuarioModel = require("../models/UsuarioModel");

//Importar el controlador de usuario
const UsuarioController = require("../controllers/UsuarioController");
//Importar el controlador de sesione
const SesionController = require("../controllers/SesionController");
import axios from "axios";


//Verifica si la tabla usuarios tiene datos
function verificarUsuarios(){
    UsuarioModel.deleteMany();

    let usuarios = UsuarioController.findAll();
    if(usuarios.then.length != 0){
        return true;
    }else{
        return false;
    }
}

//Verifica si la tabla sesiones tiene datos
function verificarSesiones(){
    let sesiones = SesionController.findAll();
    if(sesiones.then.length != 0){
        return true;
    }else{
        return false;
    }
}

function importarDatos(){
    const datos = axios.get("https://7qak3a37b4dh7kisebhllubdxq0dnehm.lambda-url.us-east-1.on.aws/");
    console.log(datos);
}

module.exports = {verificarSesiones, verificarUsuarios,importarDatos};

