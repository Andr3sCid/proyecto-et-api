const { Router } = require("express");
const router = Router();

// Controllers
const AuthController = require("../controllers/AuthController");
const SesionController = require("../controllers/SesionController");
const UsuarioController = require("../controllers/UsuarioController");
const ComprobacionController = require("../controllers/ComprobarContenidoController");
const veriyToken = require("../middlewares/verify-token");
// Rutas
router.post("/auth/register", AuthController.register);
router.post("/auth/login", AuthController.login);
router.post("/protected", veriyToken, AuthController.login);
router.get("/usuarios", UsuarioController.findAll);
router.get("/sesiones", SesionController.findAll);
router.post("/sesion/register", SesionController.create);
//router.get("/comprobacion", ComprobacionController.importarDatos);

module.exports = router;
