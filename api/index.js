const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv").config();
const app = express();
app.use(bodyParser.urlencoded({ extended: false })); // parse application/xwww-form-urlencoded
app.use(bodyParser.json()); // parse application/json
// Settings
app.set("port", process.env.PORT || 8080); 

// Connection to mongoDB
mongoose
  .connect(
    `mongodb+srv://${process.env.USER}:${process.env.PASSWORD}@proyectoetcluster.tig94vl.mongodb.net/${process.env.DBNAME}?retryWrites=true&w=majority`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("Connected database"))
  .catch((e) => console.log("DB Error:", e));

//Limpiar base de datos e importar usuarios y sesiones
// Middlewares
app.use(cors());

// Routes
app.use("/api", require("./routes"));
app.listen(app.get("port"), function () {
  console.log(`App running at: http://localhost:${app.get("port")}`);
});

module.exports = app;
