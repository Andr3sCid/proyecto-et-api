const express = require("express");
const router = express.Router();
//Importar el modelo de Sesion
const SesionModel = require("../models/SesionModels");
//Importar el modelo de Usuario
const UsuarioModel = require("../models/UsuarioModel");

//Trae todas las sesiones de un usuario determinado mediante su id
router.get("/sesiones/:id", async (req, res) => {
  try {
    const sesionesEncontradas = await SesionModel.find({ _id: req.params.id });
    res.json(sesionesEncontradas);
    //por defecto se envía una respuesta tipo 200
  } catch (error) {
    return res.status(500).json({
      mensaje: "Error al obtener las sesiones del usuario",
      error,
    });
  }
});

async function findAll(req, res) {
  try {
    const sesiones = await SesionModel.find().populate("usuario");
    res.json({ sesiones });
    //por defecto se envía una respuesta tipo 200
  } catch (error) {
    return res.status(500).json({
      mensaje: "Algo Fallo",
      error,
    });
  }
}

async function create(req, res) {
  const body = req.body;

  const nuevaSesion = new SesionModel({
    usuario: body.usuario,
    fecha: body.fecha,
    score: body.score,
    duration: body.duration,
  });
  console.log(nuevaSesion);

  SesionModel.create(nuevaSesion)
    .then(() => {
      res.status(201).send("Registro de sesion exitoso");
    })
    .catch((error) => {
      res.status(400).send({ error });
    });
}

module.exports = { router, findAll, create };
