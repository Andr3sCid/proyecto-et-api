const express = require("express");
const router = express.Router();

//Importa los servisios de ComprobacionDatos
const SesionModel = require("../models/SesionModels");
//Importar el modelo de Usuario
const UsuarioModel = require("../models/UsuarioModel");

//Importar el controlador de usuario
const UsuarioController = require("../controllers/UsuarioController");
//Importar el controlador de sesione
const SesionController = require("../controllers/SesionController");

const axios = require("axios");

const bcrypt = require("bcrypt");
const { result } = require("@hapi/joi/lib/base");

//Verifica si la tabla usuarios tiene datos
function verificarUsuarios() {
  UsuarioModel.deleteMany();

  let usuarios = UsuarioController.findAll();
  if (usuarios.then.length != 0) {
    return true;
  } else {
    return false;
  }
}

//Verifica si la tabla sesiones tiene datos
function verificarSesiones() {
  let sesiones = SesionController.findAll();
  if (sesiones.then.length != 0) {
    return true;
  } else {
    return false;
  }
}
 //Funcion para poblar la base de datos, solo usar 1
async function importarDatos(req, res) {
  // UsuarioModel.deleteMany();
  //SesionModel.deleteMany();
  const datos = await axios.get(
    "https://7qak3a37b4dh7kisebhllubdxq0dnehm.lambda-url.us-east-1.on.aws/"
  );
  try {
    datos.data.forEach( (usuario) => {
        const nuevoUsuario = new UsuarioModel({
          nombre_usuario: usuario.username,
          contrasena: "12345678",
        });
        
       UsuarioModel.findOne({ nombre_usuario: nuevoUsuario.nombre_usuario}, (err, result) =>{
          usuario.sessions.forEach((sesion) => {
            const nuevaSesion = new SesionModel({
              usuario: result._id,
              fecha: sesion.date,
              score: sesion.score,
              duration: sesion.duration,
            }); 
            SesionModel.create(nuevaSesion);
          });
        });
    });

    //por defecto se envía una respuesta tipo 200
  } catch (error) {
    return res.status(500).json({
      mensaje: "Algo Fallo",
      error,
    });
  }
}

module.exports = { verificarSesiones, verificarUsuarios, importarDatos };
