const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const joi = require("@hapi/joi");
const UsuarioModel = require("../models/UsuarioModel");

const schemaRegister = joi.object({
  nombre_usuario: joi.string().required(),
  contrasena: joi.string().min(8).max(30).required(),
});

//REGISTRAR
async function register(req, res) {
  // Validamos que los datos cumplan con la estructura del schemaRegister
  const { error } = schemaRegister.validate(req.body);

  if (error) {
    return res.status(400).json({ error: error.details[0].message });
  }

  // Validamos que el usuario no se encuentra en nuestra base de datos
  const isUserExist = await UsuarioModel.findOne({
    nombre_usuario: req.body.nombre_usuario,
  });

  if (isUserExist) {
    return res.status(400).json({ error: "Usuario ya registrado" });
  }

  // Encriptamos la contraseña
  const salt = await bcrypt.genSalt(10);
  const password = await bcrypt.hash(req.body.contrasena, salt);

  const nuevoUsuario = new UsuarioModel({
    nombre_usuario: req.body.nombre_usuario,
    contrasena: password,
  });

  UsuarioModel.create(nuevoUsuario)
    .then(() => {
      res.status(201).send("Registro de usuario exitoso");
    })
    .catch((error) => {
      res.status(400).send({ error });
    });
}

//INICIO SESION

const schemaLogin = joi.object({
  nombre_usuario: joi.string().required(),
  contrasena: joi.string().min(8).max(30).required(),
});

async function login(req, res) {
  // Validamos los datos
  const { error } = schemaLogin.validate(req.body);

  if (error) return res.status(400).json({ error: error.details[0].message });

  // Buscamos el usuario en la base de datos
  const usuario = await UsuarioModel.findOne({
    nombre_usuario: req.body.nombre_usuario,
  });

  if (!usuario) return res.status(400).json({ error: "Usuario no encontrado" });

  const validPassword = await bcrypt.compare(
    req.body.contrasena,
    usuario.contrasena
  );

  if (!validPassword)
    return res.status(400).json({ error: "Contraseña incorrecta" });

  // Se crea el token
  const token = jwt.sign(
    {
      name: usuario.nombre_usuario,
      id: usuario._id,
    },
    process.env.TOKEN_SECRET
  );

  res.json({ usuario, token });
}

module.exports = {
  register,
  login,
};
