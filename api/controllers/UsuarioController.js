const express = require("express");
const router = express.Router();
//Importar el modelo de Usuario
const UsuarioModel = require("../models/UsuarioModel");

//Agregar
router.post("/new-persona", async (req, res) => {
  const body = req.body;
  try {
    const personaDB = await UsuarioModel.create(body);
    //se espera una "promesa", hace referencia a la petición
    //solicitada a servidor.
    //await, es funciones asíncronas (async) esta
    //expresión pausa el proceso
    //hasta que se reciba o rechace la promesa.
    //Importante, si la recepción de status, la peticion se mantiene
    //a la espera indeterminadamente
    res.status(200).json(personaDB);
  } catch (error) {
    return res.status(500).json({
      mensaje: "Algo fallo",
      error,
    });
    //error 500, falla con el servidor
  }
});

//Búsqueda por id
router.get("/persona/:id", async (req, res) => {
  const _id = req.params.id;
  try {
    const personaDB = await UsuarioModel.findOne({ _id });
    res.status(200).json(personaDB);
  } catch (error) {
    return res.status(400).json({
      mensaje: "Algo Fallo",
      error,
    });
  }
});

//Obtener todo los documentos
async function findAll(req, res) {
  try {
    const usuarios = await UsuarioModel.find();
    res.json({usuarios});
    //por defecto se envía una respuesta tipo 200
  } catch (error) {
    return res.status(500).json({
      mensaje: "Algo Fallo",
      error,
    });
  }
}

//Eliminar documento específico.
router.delete("/persona/:id", async (req, res) => {
  const _id = req.params.id;
  try {
    const personaDB = await UsuarioModel.findByIdAndDelete({ _id });
    if (!personaDB) {
      return res.status(400).json({
        mensaje: "No se encuentra el ID",
      });
    }
    res.json(personaDB);
  } catch (error) {
    return res.status(400).json({
      mensaje: "Algo Fallo",
      error,
    });
  }
});

//Actualizar Documento especifico
router.put("/persona/:id", async (req, res) => {
  const _id = req.params.id;
  const body = req.body;
  try {
    const personaDB = await UsuarioModel.findByIdAndUpdate({ _id }, body, {
      new: true,
    });
    if (!personaDB) {
      return res.status(400).json({
        mensaje: "No se encuentra el ID",
      });
    }
    res.json(personaDB);
  } catch (error) {
    return res.status(400).json({
      mensaje: "Algo Fallo",
      error,
    });
  }
});

module.exports = {router,findAll,};
